import json
# writen by Ziegler 10.12.19 

# the json obj 
data = {}
# adding a 'status' array element to the json obj
data['status'] = []
# add one 'status' obj to the array
data['status'].append({
    'mood': 0,
    'food': 0,
    'hp': 0 })
# lets just add one more 'status' obj to the array
data['status'].append({
    'mood': 10,
    'food': 10,
    'hp': 10 })

# we open a file called 'data.txt', (and we will call it outfile in this code "as outfile: ")
# we then dump our json data into this file
# the w in [open('data.txt', 'w')] stands for "write".... I think xD
with open('data.txt', 'w') as outfile:
    json.dump(data, outfile)
    
