from sense_hat import SenseHat
import time

s = SenseHat()
s.low_light = True

green = (0, 255, 0)
yellow = (255, 255, 0)
blue = (0, 0, 255)
red = (255, 0, 0)
white = (255,255,255)
nothing = (0,0,0)
pink = (255,105, 180)

# grids made by Maher
def mad_pixels():
    G = green
    Y = yellow
    B = blue
    O = nothing
    B = blue
    W = white
    R = red
    photo1 = [
    B, B, B, B, B, B, B, B,
    B, R, R, B, B, R, R, B,
    B, R, R, B, B, R, R, B,
    B, B, B, B, B, B, B, B,
    B, B, B, B, B, B, B, B,
    B, R, R, R, R, R, R, B,
    B, B, B, B, B, B, B, B,
    B, B, B, B, B, B, B, B,
    ]
    photo2 = [
    B, B, B, B, B, B, B, B,
    B, R, R, B, B, R, R, B,
    B, R, R, B, B, R, R, B,
    B, B, B, B, B, B, B, B,
    B, B, R, R, R, R, B, B,
    B, R, R, R, R, R, R, B,
    B, R, R, B, B, R, R, B,
    B, R, B, B, B, B, R, B,
    ]

    photos = [photo1, photo2]
    return photos

# grids made by Maher
def hunger1_pixels():
    G = green
    Y = yellow
    B = blue
    O = nothing
    B = blue
    W = white
    R = red
    photo1 = [
    B, B, B, B, B, B, B, B,
    B, Y, Y, B, B, Y, Y, B,
    B, Y, Y, B, B, Y, Y, B,
    B, B, B, B, B, B, B, B,
    B, Y, Y, Y, Y, Y, Y, B,
    B, B, Y, O, O, Y, B, B,
    B, B, B, Y, Y, B, B, B,
    B, B, B, B, B, B, B, B,
    ]
    photo2 = [
    B, B, B, B, B, B, B, B,
    B, Y, Y, B, B, Y, Y, B,
    B, Y, Y, B, B, Y, Y, B,
    B, B, B, B, B, B, B, B,
    B, Y, Y, Y, Y, Y, Y, B,
    B, Y, O, O, O, O, Y, B,
    B, B, Y, O, O, Y, B, B,
    B, B, B, Y, Y, B, B, B,
    ]

    photos = [photo1, photo2]
    return photos

# grids made by Nicolai
def hunger2_pixels():
    G = green
    Y = yellow
    B = blue
    O = nothing
    B = blue
    W = white
    R = red
    photo1 = [
    B, B, B, B, B, B, B, B,
    B, W, W, B, B, W, W, B,
    B, W, W, B, B, W, W, B,
    B, B, B, B, B, B, B, B,
    B, W, W, W, W, W, W, B,
    B, B, W, O, O, W, B, B,
    B, B, B, W, W, B, B, B,
    B, B, B, B, B, B, B, B,

    ]
    photo2 = [
    B, B, B, B, B, B, B, B,
    B, W, W, B, B, W, W, B,
    B, W, W, B, B, W, W, B,
    B, B, B, B, B, B, B, B,
    B, W, W, W, W, W, W, B,
    B, W, O, O, O, O, W, B,
    B, B, W, O, O, W, B, B,
    B, B, B, W, W, B, B, B,

    ]

    photos = [photo1, photo2]
    return photos

# grids made by Nicolai
def bone_pixels():
    G = green
    Y = yellow
    B = blue
    O = nothing
    B = blue
    W = white
    R = red
    photo1 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, W, O, O,
    O, O, O, O, O, W, W, O,
    O, O, O, O, W, O, O, O,
    O, O, O, W, O, O, O, O,
    O, W, W, O, O, O, O, O,
    O, O, W, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    ]
    photo2 = [
    O, O, O, O, O, O, O, O,
    O, O, W, O, O, O, O, O,
    O, W, W, O, O, O, O, O,
    O, O, O, W, O, O, O, O,
    O, O, O, O, W, O, O, O,
    O, O, O, O, O, W, W, O,
    O, O, O, O, O, W, O, O,
    O, O, O, O, O, O, O, O,

    ]

    photos = [photo1, photo2]
    return photos

# grids made by Nicolai
def ball_pixels():
    G = green
    Y = yellow
    B = blue
    O = nothing
    R = red
    W = white
    lightred = (255, 90, 90)
    darkred = (155, 0, 0)
    LR = lightred
    DR = darkred
    photo1 = [
    O, O, O, O, O, O, O, O,
    O, O, O, R, R, O, O, O,
    O, O, R, R, LR, LR, O, O,
    O, DR, R, R, R, LR, R, O,
    O, DR, R, R, R, R, R, O,
    O, O, R, R, R, R, O, O,
    O, O, O, R, R, O, O, O,
    O, O, O, O, O, O, O, O,
    ]
    photos = [photo1]
    return photos

# grids made by Gitte
def happy2_pixels():
    w = (255, 255, 255)
    g = (50, 205, 50)
    c = (0, 0, 255)
    photo1 = [
    c, c, c, c, c, c, c, c,
    c, g, g, c, c, g, g, c,
    c, g, g, c, c, g, g, c,
    c, c, c, c, c, c, c, c,
    c, g, c, c, c, c, g, c,
    c, c, g, g, g, g, c, c,
    c, c, c, c, c, c, c, c,
    c, c, c, c, c, c, c, c
    ]

    photos = [photo1]
    return photos

# grids made by Gitte
def happy_pixels():
    w = (255, 255, 255)
    G = (50, 205, 50)
    B = blue
    photo1 = [
    B, B, B, B, B, B, B, B,
    B, G, G, B, B, G, G, B,
    B, G, G, B, B, G, G, B,
    B, B, B, B, B, B, B, B,
    B, B, B, B, B, B, B, B,
    B, G, B, B, B, B, G, B,
    B, B, G, G, G, G, B, B,
    B, B, B, B, B, B, B, B,
    ]

    photo2 = [
    B, B, B, B, B, B, B, B,
    B, G, G, B, B, G, G, B,
    B, G, G, B, B, G, G, B,
    B, B, B, B, B, B, B, B,
    B, G, B, B, B, B, G, B,
    B, G, G, G, G, G, G, B,
    B, B, G, G, G, G, B, B,
    B, B, B, B, B, B, B, B,
    ]

    photos = [photo1, photo2]
    return photos

# grids made by Nicolai
def bouncingBall():
    G = green
    Y = yellow
    B = blue
    O = nothing
    R = red
    W = white
    lightred = (255, 90, 90)
    darkred = (155, 0, 0)
    LR = lightred
    DR = darkred
    photo3 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, R, R, R, R, O, O,
    O, R, R, R, LR, LR, R, O,
    O, R, R, R, R, LR, R, O,
    O, R, R, R, R, R, R, O,
    O, R, R, R, R, R, R, O,
    O, O, R, R, R, R, O, O,
    ]
    photo1 = [
    O, O, R, R, R, R, O, O,
    O, R, R, R, LR, LR, R, O,
    O, R, R, R, R, LR, R, O,
    O, R, R, R, R, R, R, O,
    O, R, R, R, R, R, R, O,
    O, O, R, R, R, R, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    ]
    photo2 = [
    O, O, R, R, R, R, O, O,
    O, R, R, R, LR, LR, R, O,
    O, R, R, R, R, LR, R, O,
    O, R, R, R, R, R, R, O,
    O, R, R, R, R, R, R, O,
    O, O, R, R, R, R, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    ]
    photos = [photo1, photo2, photo3]
    return photos

