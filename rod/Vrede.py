from sense_hat import SenseHat
import time

s = SenseHat()
s.low_light = True

green = (0, 255, 0)
yellow = (255, 255, 0)
blue = (0, 0, 255)
red = (255, 0, 0)
white = (255,255,255)
nothing = (0,0,0)
pink = (255,105, 180)

def vred_logo():
    G = green
    Y = yellow
    B = blue
    O = nothing
    B = blue
    W = white
    R = red
    logo = [
    B, B, B, B, B, B, B, B,
    B, R, R, B, B, R, R, B,
    B, R, R, B, B, R, R, B,
    B, B, B, B, B, B, B, B,
    B, B, B, B, B, B, B, B,
    B, R, R, R, R, R, R, B,
    B, B, B, B, B, B, B, B,
    B, B, B, B, B, B, B, B,

    ]
    return logo

def vred2_logo():
    G = green
    R = red
    O = nothing
    W = white
    B = blue
    logo = [
    B, B, B, B, B, B, B, B,
    B, R, R, B, B, R, R, B,
    B, R, R, B, B, R, R, B,
    B, B, B, B, B, B, B, B,
    B, B, R, R, R, R, B, B,
    B, R, R, R, R, R, R, B,
    B, R, R, B, B, R, R, B,
    B, R, B, B, B, B, R, B,
    ]
    return logo




images = [vred_logo, vred_logo,vred2_logo, vred2_logo,]
count = 0

while True: 
    s.set_pixels(images[count % len(images)]())
    time.sleep(.75)
    count += 1