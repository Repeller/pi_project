from sense_hat import SenseHat
import time

s = SenseHat()
s.low_light = True

green = (50, 205, 50)
yellow = (255, 255, 0)
blue = (0, 0, 255)
red = (255, 0, 0)
white = (255,255,255)
nothing = (0,0,0)
pink = (255,105, 180)


def trinket_logo1():
    G = green
    Y = yellow
    B = blue
    O = nothing
    logo = [
  B,B,B,B,B,B,B,B,
  B,G,G,B,B,G,G,B,
  B,G,G,B,B,G,G,B,
  B,B,B,B,B,B,B,B,
  B,B,B,B,B,B,B,B,
  B,G,B,B,B,B,G,B,
  B,B,G,G,G,G,B,B,
  B,B,B,B,B,B,B,B,
    ]
    return logo
    
def trinket_logo2():
    G = green
    Y = yellow
    B = blue
    O = nothing
    logo = [
  B,B,B,B,B,B,B,B,
  B,G,G,B,B,G,G,B,
  B,G,G,B,B,G,G,B,
  B,B,B,B,B,B,B,B,
  B,G,B,B,B,B,G,B,
  B,G,G,G,G,G,G,B,
  B,B,G,G,G,G,B,B,
  B,B,B,B,B,B,B,B,
    ]
    return logo

images = [trinket_logo1, trinket_logo2]
count = 0

while True: 
    s.set_pixels(images[count % len(images)]())
    time.sleep(.75)
    count += 1