from sense_hat import SenseHat
import time

s = SenseHat()
s.low_light = True

green = (0, 255, 0)
yellow = (255, 255, 0)
blue = (0, 0, 255)
red = (255, 0, 0)
white = (255,255,255)
nothing = (0,0,0)
pink = (255,105, 180)

def trinket_logo():
    G = green
    Y = yellow
    B = blue
    O = nothing
    R = red
    W = white
    logo = [
    B, B, B, B, B, B, B, B,
    B, W, W, B, B, W, W, B,
    B, W, W, B, B, W, W, B,
    B, B, B, B, B, B, B, B,
    B, W, W, W, W, W, W, B,
    B, B, W, O, O, W, B, B,
    B, B, B, W, W, B, B, B,
    B, B, B, B, B, B, B, B,
    ]
    return logo

    
def trinket_sult():
    G = green
    Y = yellow
    B = blue
    O = nothing
    R = red
    W = white
    logo = [
    B, B, B, B, B, B, B, B,
    B, W, W, B, B, W, W, B,
    B, W, W, B, B, W, W, B,
    B, B, B, B, B, B, B, B,
    B, W, W, W, W, W, W, B,
    B, W, O, O, O, O, W, B,
    B, B, W, O, O, W, B, B,
    B, B, B, W, W, B, B, B,
    ]
    return logo


images = [trinket_logo, trinket_sult]
count = 0

while True: 
    s.set_pixels(images[count % len(images)]())
    time.sleep(.75)
    count += 1
